package jp.sf.pal.helloworld;

import java.io.IOException;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.GenericPortlet;
import javax.portlet.PortletConfig;
import javax.portlet.PortletContext;
import javax.portlet.PortletException;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

























public class HelloWorldPortlet
  extends GenericPortlet
{
  private static final Log log = LogFactory.getLog(HelloWorldPortlet.class);
  
  public static final String YOUR_NAME_KEY = "yourName";
  public static final String COUNT_KEY = "count";
  
  public HelloWorldPortlet() {}
  
  public void init(PortletConfig config)
    throws PortletException
  {
    if (log.isDebugEnabled())
    {
      log.debug("init() - HelloWorldPortlet: init()");
    }
    super.init(config);
  }
  


  protected void doView(RenderRequest request, RenderResponse response)
    throws PortletException, IOException
  {
    response.setContentType("text/html");
    
    if (log.isDebugEnabled())
    {
      log.debug("doView() - HelloWorldPortlet: doView()");
    }
    PortletContext context = getPortletContext();
    
    String yourName = request.getParameter("yourName");
    if (log.isDebugEnabled())
    {
      log.debug("doView() - yourName=" + yourName);
    }
    if (yourName == null)
    {
      yourName = "";
    }
    request.setAttribute("yourName", yourName);
    
    PortletRequestDispatcher rd = context.getRequestDispatcher("/WEB-INF/view/helloworld.jsp");
    rd.include(request, response);
  }
  


  public void processAction(ActionRequest request, ActionResponse response)
    throws PortletException, IOException
  {
    if (log.isDebugEnabled())
    {
      log.debug("processAction() - HelloWorldPortlet: processAction()");
    }
    PortletSession session = request.getPortletSession();
    String yourName = request.getParameter("yourName");
    
    if (yourName != null)
    {
      Integer count = (Integer)session.getAttribute("count");
      if (count != null)
      {
        count = new Integer(count.intValue() + 1);
      }
      else
      {
        count = new Integer(1);
      }
      if (log.isDebugEnabled())
      {
        log.debug("processAction() - HelloWorldPortlet: yourName = " + yourName + ", count = " + count);
      }
      
      session.setAttribute("count", count);
      response.setRenderParameter("yourName", yourName);
    }
  }
}
